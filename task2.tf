# AWS Provider with the region
provider "aws" {
  region  = "us-east-1"
}

#resources to zip the lambda function
data "aws_caller_identity" "current" {}
data "aws_region" "current" {}

data "archive_file" "create_user" {
  type        = "zip"
  source_file = "lambda_functions/create_user.py"
  output_path = "lambda_functions/create_user.zip"
}
data "archive_file" "delete_user" {
  type        = "zip"
  source_file = "lambda_functions/delete_user.py"
  output_path = "lambda_functions/delete_user.zip"
}
data "archive_file" "create_group" {
  type        = "zip"
  source_file = "lambda_functions/create_group.py"
  output_path = "lambda_functions/create_group.zip"
}
data "archive_file" "update_user" {
  type        = "zip"
  source_file = "lambda_functions/update_user.py"
  output_path = "lambda_functions/update_user.zip"
}
#User pool
resource "aws_cognito_user_pool" "pool" {
  name = "testpool"
}
#IAM role and policy for lambda functions
resource "aws_iam_role" "iam_for_lambda" {
  name = "iam_for_lambda"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "lambda.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF
}
resource "aws_iam_policy" "lambda_policy" {
  name        = "lambda_policy"
  path        = "/"

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": [
        "*"
      ],
      "Resource": "*",
      "Effect": "Allow"
    }
  ]
}
EOF
}
resource "aws_iam_role_policy_attachment" "lambda_association" {
  role       = aws_iam_role.iam_for_lambda.name
  policy_arn = aws_iam_policy.lambda_policy.arn
}

#API
resource "aws_api_gateway_rest_api" "api" {
  name = "testapi"
}

#Resources for APIs
resource "aws_api_gateway_resource" "create_user_resource" {
  path_part   = "create_user"
  parent_id   = aws_api_gateway_rest_api.api.root_resource_id
  rest_api_id = aws_api_gateway_rest_api.api.id
}

resource "aws_api_gateway_resource" "update_user_resource" {
  path_part   = "update_user"
  parent_id   = aws_api_gateway_rest_api.api.root_resource_id
  rest_api_id = aws_api_gateway_rest_api.api.id
}

resource "aws_api_gateway_resource" "delete_user_resource" {
  path_part   = "delete_user"
  parent_id   = aws_api_gateway_rest_api.api.root_resource_id
  rest_api_id = aws_api_gateway_rest_api.api.id
}

resource "aws_api_gateway_resource" "create_group_resource" {
  path_part   = "create_group"
  parent_id   = aws_api_gateway_rest_api.api.root_resource_id
  rest_api_id = aws_api_gateway_rest_api.api.id
}
#Api Methods
resource "aws_api_gateway_method" "create_user_method" {
  rest_api_id   = aws_api_gateway_rest_api.api.id
  resource_id   = aws_api_gateway_resource.create_user_resource.id
  http_method   = "POST"
  authorization = "NONE"
}

resource "aws_api_gateway_method" "delete_method" {
  rest_api_id   = aws_api_gateway_rest_api.api.id
  resource_id   = aws_api_gateway_resource.delete_user_resource.id
  http_method   = "POST"
  authorization = "NONE"
}

resource "aws_api_gateway_method" "create_group_method" {
  rest_api_id   = aws_api_gateway_rest_api.api.id
  resource_id   = aws_api_gateway_resource.create_group_resource.id
  http_method   = "POST"
  authorization = "NONE"
}

resource "aws_api_gateway_method" "update_user_method" {
  rest_api_id   = aws_api_gateway_rest_api.api.id
  resource_id   = aws_api_gateway_resource.update_user_resource.id
  http_method   = "POST"
  authorization = "NONE"
}

#Integration of resource and methods
resource "aws_api_gateway_integration" "integration_create_user" {
  rest_api_id             = aws_api_gateway_rest_api.api.id
  resource_id             = aws_api_gateway_resource.create_user_resource.id
  http_method             = aws_api_gateway_method.create_user_method.http_method
  integration_http_method = "POST"
  type                    = "AWS_PROXY"
  uri                     = aws_lambda_function.lambda_create_user.invoke_arn
}

resource "aws_api_gateway_integration" "integration_delete_user" {
  rest_api_id             = aws_api_gateway_rest_api.api.id
  resource_id             = aws_api_gateway_resource.delete_user_resource.id
  http_method             = aws_api_gateway_method.delete_method.http_method
  integration_http_method = "POST"
  type                    = "AWS_PROXY"
  uri                     = aws_lambda_function.lambda_delete_user.invoke_arn
}

resource "aws_api_gateway_integration" "integration_create_group" {
  rest_api_id             = aws_api_gateway_rest_api.api.id
  resource_id             = aws_api_gateway_resource.create_group_resource.id
  http_method             = aws_api_gateway_method.create_group_method.http_method
  integration_http_method = "POST"
  type                    = "AWS_PROXY"
  uri                     = aws_lambda_function.lambda_create_group.invoke_arn
}

resource "aws_api_gateway_integration" "integration_update_user" {
  rest_api_id             = aws_api_gateway_rest_api.api.id
  resource_id             = aws_api_gateway_resource.update_user_resource.id
  http_method             = aws_api_gateway_method.update_user_method.http_method
  integration_http_method = "POST"
  type                    = "AWS_PROXY"
  uri                     = aws_lambda_function.lambda_update_user.invoke_arn
}

# Lambda Permission to use APIGW
resource "aws_lambda_permission" "apigw_lambda_create_user" {
  statement_id  = "AllowExecutionFromAPIGateway"
  action        = "lambda:InvokeFunction"
  function_name = aws_lambda_function.lambda_create_user.function_name
  principal     = "apigateway.amazonaws.com"
  source_arn = "arn:aws:execute-api:${data.aws_region.current.name}:${data.aws_caller_identity.current.account_id}:${aws_api_gateway_rest_api.api.id}/*/${aws_api_gateway_method.create_user_method.http_method}${aws_api_gateway_resource.create_user_resource.path}"
}

resource "aws_lambda_permission" "apigw_lambda_delete" {
  statement_id  = "AllowExecutionFromAPIGateway"
  action        = "lambda:InvokeFunction"
  function_name = aws_lambda_function.lambda_delete_user.function_name
  principal     = "apigateway.amazonaws.com"
  source_arn = "arn:aws:execute-api:${data.aws_region.current.name}:${data.aws_caller_identity.current.account_id}:${aws_api_gateway_rest_api.api.id}/*/${aws_api_gateway_method.delete_method.http_method}${aws_api_gateway_resource.delete_user_resource.path}"
}

resource "aws_lambda_permission" "apigw_lambda_create_group" {
  statement_id  = "AllowExecutionFromAPIGateway"
  action        = "lambda:InvokeFunction"
  function_name = aws_lambda_function.lambda_create_group.function_name
  principal     = "apigateway.amazonaws.com"
  source_arn = "arn:aws:execute-api:${data.aws_region.current.name}:${data.aws_caller_identity.current.account_id}:${aws_api_gateway_rest_api.api.id}/*/${aws_api_gateway_method.create_group_method.http_method}${aws_api_gateway_resource.create_group_resource.path}"
}

resource "aws_lambda_permission" "apigw_lambda_update_user" {
  statement_id  = "AllowExecutionFromAPIGateway"
  action        = "lambda:InvokeFunction"
  function_name = aws_lambda_function.lambda_update_user.function_name
  principal     = "apigateway.amazonaws.com"
  source_arn = "arn:aws:execute-api:${data.aws_region.current.name}:${data.aws_caller_identity.current.account_id}:${aws_api_gateway_rest_api.api.id}/*/${aws_api_gateway_method.update_user_method.http_method}${aws_api_gateway_resource.update_user_resource.path}"
}

#Lambdas
resource "aws_lambda_function" "lambda_create_user" {
  filename      = "lambda_functions/create_user.zip"
  function_name = "create-user"
  role          = aws_iam_role.iam_for_lambda.arn
  handler       = "create_user.lambda_handler"
  runtime = "python3.7"

}

resource "aws_lambda_function" "lambda_delete_user" {
  filename      = "lambda_functions/delete_user.zip"
  function_name = "delete-user"
  role          = aws_iam_role.iam_for_lambda.arn
  handler       = "delete_user.lambda_handler"
  runtime = "python3.7"

}

resource "aws_lambda_function" "lambda_create_group" {
  filename      = "lambda_functions/create_group.zip"
  function_name = "create_group"
  role          = aws_iam_role.iam_for_lambda.arn
  handler       = "create_group.lambda_handler"
  runtime = "python3.7"

}

resource "aws_lambda_function" "lambda_update_user" {
  filename      = "lambda_functions/update_user.zip"
  function_name = "update_user"
  role          = aws_iam_role.iam_for_lambda.arn
  handler       = "update_user.lambda_handler"
  runtime = "python3.7"

}
