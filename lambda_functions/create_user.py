import json
import boto3

client = boto3.client('cognito-idp')

def lambda_handler(event, context):
    data = json.loads(event['body'])
    name = data['name']
    email = data['email']
    username = data['username']
    phone_number = data['phone_number']
    temporary_password = data['temporary_password']
    user_pool_id = data['user_pool_id']

    response = client.admin_create_user(
        UserPoolId= user_pool_id,
        Username='test',
        UserAttributes=[
        {
            'Name': 'name',
            'Value': name
        },
        {
            'Name': 'email',
            'Value': email
        },
        {
            'Name': 'phone_number',
            'Value': phone_number
        }
        ],
        TemporaryPassword= temporary_password,
        DesiredDeliveryMediums=['EMAIL']
    )

    # TODO implement
    return {
        'statusCode': 200,
        'body': json.dumps('User Created!')
    }
