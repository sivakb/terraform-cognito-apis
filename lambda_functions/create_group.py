import json
import boto3

client = boto3.client('cognito-idp')

def lambda_handler(event, context):
    data = json.loads(event['body'])
    print(data)
    user_pool_id = data['user_pool_id']
    group_name = data ['group_name']
    response = client.create_group(
    GroupName=group_name,
    UserPoolId=user_pool_id)
    return {
        'statusCode': 200,
        'body': json.dumps('Group Created!')
    }
