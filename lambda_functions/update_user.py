import json
import boto3

client = boto3.client('cognito-idp')

def lambda_handler(event, context):
    data = json.loads(event['body'])
    user_pool_id = data['user_pool_id']
    username = data['username']
    name = data['name']
    email = data['email']
    phone_number = data['phone_number']
    response = client.admin_update_user_attributes(
    UserPoolId=user_pool_id,
    Username=username,
    UserAttributes=[
        {
            'Name': 'name',
            'Value': name
        },
        {
            'Name': 'email',
            'Value': email
        },
        {
            'Name': 'phone_number',
            'Value': phone_number
        }
    ])
    return {
        'statusCode': 200,
        'body': json.dumps('User updated!')
    }
