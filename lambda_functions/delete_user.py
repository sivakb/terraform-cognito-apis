import json
import boto3

client = boto3.client('cognito-idp')

def lambda_handler(event, context):
    data = json.loads(event['body'])
    user_pool_id = data['user_pool_id']
    username = data['username']
    response = client.admin_delete_user(
    UserPoolId = user_pool_id,
    Username = username
    )
    return {
        'statusCode': 200,
        'body': json.dumps('User deleted!')
    }
